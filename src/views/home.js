import React, {Component} from 'react';
import Fields from './fields';
import List from './list';

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            term: '',
            items: []
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange = (event) => {
        console.log('1');
        this.setState({
            items: event.target.value
        });
    }

    onChange = (event) => {
        this.setState({
            term: event.target.value
        })
    };

    onSubmit = (event) => {
        event.preventDefault();
        this.setState({
            term: '',
            items: [...this.state.items, this.state.term]
        });
    };

    render() {
        return (
            <div>
                <form className="App" onSubmit={this.onSubmit}>
                    <input value={this.state.term} onChange={this.onChange}/>
                    <button>Добавить</button>
                <Fields items={this.state.items}/>
                </form>
                <List items={this.state.items}/>
            </div>
        );
    }
}