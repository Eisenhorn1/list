import React from 'react';

const Fields = ({items}) => (
    <div>
    {
        items && items.map((item, index) => <textarea rows="5" key={index}>{item}</textarea>)
    }
    </div>
);

export default Fields;